import math
import itertools as it

import theano
import theano.tensor as T
import theano.tensor.nnet
import numpy as np

#num_filters, dimension

DEFAULT_CONFIG = (
        (
            (32, 1),
            (16, 3),
            (8, 5),
            (4, 7)
        ),
)


class ADAMLayer:
    def __init__(self, dim, beta1, beta2, eta):
        self.v = theano.shared(
                value = np.zeros(
                    (dim),
                    dtype = theano.config.floatX
                ),
                name = "v",
                borrow = True)

        self.m = theano.shared(
                value = np.zeros(
                    (dim),
                    dtype = theano.config.floatX
                ),
                name = "m",
                borrow = True)

        self.beta1 = beta1
        self.beta2 = beta2

        self.eta = eta

    def updates(self, var, grad, t):
        adjusted_m = self.m / (1.0 - self.beta1 ** (t + 1))
        adjusted_v = self.v / (1.0 - self.beta2 ** (t + 1))

        return [
            (self.m, self.beta1 * self.m + (1.0 - self.beta1) * grad),
            (self.v, self.beta2 * self.v + (1.0 - self.beta2) * grad * grad),
            (var, var - (self.eta / (adjusted_v + 10e-6)) * adjusted_m)]


class TheanoWordCNNModel:
    def __init__(self, num_cls, layer_config = DEFAULT_CONFIG, lr = 0.01, hidden_layer_size = 512, max_epochs = 50, beta1 = 0.5, beta2 = 0.5, eta = 0.01):
       # self.conv_layer_configs = [
            #num_filters, dimension
       #     (
       #         (32, 1),
       #         (16, 3),
       #         (8, 5),
       #         (4, 7)
       #     )
       # ]

        self.conv_layer_configs = layer_config

        self.updateables = []

        self.num_cls = num_cls
        self.lr = lr

        self.hidden_layer_size = hidden_layer_size

        self.max_epochs = max_epochs

        self.beta1 = beta1
        self.beta2 = beta2
        self.eta = eta

    def build(self, input_shape):
        self.X = T.tensor4()
        self.y = T.imatrix()
        self.t = T.scalar()
        
        self.conv_layers = [[(self.X, (1,) + input_shape)]]

        for config in self.conv_layer_configs:
            layer = []
            for layer_elem, dim in self.conv_layers[-1]:
                layer.append(self.build_conv_layer(layer_elem, dim, config))

            self.conv_layers.append(layer)

        self.max_pooling_values = []

        num_pools = 0
        for layer_elem, dim in self.conv_layers[-1]:
            per_filter_maximums = layer_elem.max(axis = (2, 3))

            self.max_pooling_values.append(T.flatten(per_filter_maximums, 2))

            num_pools += dim[0]

        
        self.pre_fc_layer = T.concatenate(self.max_pooling_values, axis = 1)

        self.fc1 = self.build_fc_layer(self.pre_fc_layer, num_pools, self.hidden_layer_size)

        #self.fc2 = self.build_fc_layer(self.fc1, 512, 512)
        self.classification = self.build_fc_layer(self.fc1, self.hidden_layer_size, self.num_cls, T.nnet.nnet.sigmoid)
        self.loss = T.sum(T.nnet.nnet.binary_crossentropy(self.classification, self.y)) / (self.classification.shape[0] * self.classification.shape[1]) + T.sum(self.classification ** 2)

        #for updateable_var, dim in self.updateables:
        #    self.loss += T.sum(updateable_var ** 2)
        #self.loss = -(T.log((self.classification)) * (self.y) + T.log((1.0 - self.classification)) * (1.0 - self.y)))
        #self.loss = [T.log(self.classification)]

    def run(self, train_X, train_y, batch_size = 128):
        num_batches = int(math.ceil(train_X.shape[0] / batch_size))

        train_X = np.expand_dims(train_X, 1)
        train_X, train_y = upload_data(train_X, train_y)

        batch_idx_var = T.lscalar()
        train_model = theano.function(
            inputs=[batch_idx_var, self.t],
            outputs=[self.loss],
            updates = self.compute_updates(),
            givens = {
                self.X: train_X[batch_idx_var * batch_size : (batch_idx_var + 1) * batch_size],
                self.y: train_y[batch_idx_var * batch_size : (batch_idx_var + 1) * batch_size]    
        })

        best_values = {}
        best_loss = None

        for epoch in range(self.max_epochs):
            loss = 0
            for batch_idx in range(num_batches):
                batch_loss, = train_model(batch_idx, epoch)

                loss += batch_loss
            
            print(loss)
            if best_loss is None or loss < best_loss:
                for shared_var, _ in self.updateables:
                    best_values[shared_var] = shared_var.get_value()

                best_loss = loss

        for var, val in best_values.items():
            var.set_value(val)

        print()

    def predict(self, test_X, test_y, batch_size = 32):
        num_batches = int(math.ceil(test_X.shape[0] / batch_size))

        #test_X = np.array([[[float(x % 100) for i in range(50)] for _ in range(500)] for x in range(340)])
        #print(test_X.shape)
        test_X = np.expand_dims(test_X, 1)
        #test_X, test_y = upload_data(test_X, test_y)
        test_X, test_y = upload_data(test_X, np.array([]))

        batch_idx_var = T.lscalar()
        
        test_model = theano.function(
            inputs=[batch_idx_var],
            outputs=self.classification,
            givens = {
                self.X: test_X[batch_idx_var * batch_size : (batch_idx_var + 1) * batch_size],
        })


        classification_results = []
        for batch_idx in range(num_batches):
            result = test_model(batch_idx)
            classification_results.append(result)

        res = np.concatenate(classification_results)

        #print("Pred", res, np.max(res), np.sum(res))
        return res

    def compute_updates(self):
        updates = []
        for updateable_var, dim in self.updateables:
            grad = T.grad(cost = self.loss, wrt = updateable_var)
            adam = ADAMLayer(dim, self.beta1, self.beta2, self.eta)
            updates.extend(adam.updates(updateable_var, grad, self.t))
            #updates.append((updateable_var, updateable_var - self.lr * grad))

        return updates

    def build_fc_layer(self, X, input_dim, num_neurons, f_act = T.nnet.relu):
        W = theano.shared(
                value = np.random.uniform(size = (input_dim, num_neurons)).astype(theano.config.floatX)
        )
        b = theano.shared(
            value = np.zeros((num_neurons,), dtype = theano.config.floatX)
        )
        activation = f_act(T.dot(X, W) + b)

        self.updateables.append((W, (input_dim, num_neurons)))
        self.updateables.append((b, (num_neurons,)))

        return activation

    def build_conv_layer(self, X, input_dim, config):
        input_channels, input_rows = input_dim

        convolution_operations = []

        filter_cnt = 0
        num_pads = 0
        prev_filter_size = None
        for num_filters, filter_size in config:
            filters_var = theano.shared(
                    value = np.random.uniform(size = (num_filters, input_channels, filter_size, input_rows)).astype(theano.config.floatX)
            )

            if prev_filter_size is not None:
                num_pads += (filter_size - prev_filter_size) / 2

            prev_filter_size = filter_size

            conv2d = T.nnet.relu(T.nnet.conv2d(X, filters_var, border_mode = (num_pads, 0)))


            #convolution_operations.append(conv2d, (num_filters, 1, input_rows - filter_size + 1)))
            convolution_operations.append(conv2d)

            
            filter_cnt += num_filters

            self.updateables.append((filters_var, (num_filters, input_channels, filter_size, input_rows)))

        #print(theano.function([X], [T.transpose(T.concatenate(convolution_operations, axis = 1), (0, 3, 2, 1)).shape])(np.expand_dims(np.array([[[10.0 for i in range(input_rows)] for i in range(10)]]), 1).astype(theano.config.floatX)))
        return T.transpose(T.concatenate(convolution_operations, axis = 1), (0, 3, 2, 1)), (1, filter_cnt)


def load_sentence_vectors(emb_size, n, loc):
    """
    Load vectors for sentence representation
    """
    return np.random.normal(size = (n, emb_size), loc = loc).astype(theano.config.floatX)

def load_random_data(n_pos_docs, n_neg_docs):
    pos_docs = []
    for _ in range(n_pos_docs):
        pos_docs.append(load_sentence_vectors(128, 25, 0.5))

    neg_docs = []
    for _ in range(n_neg_docs):
        neg_docs.append(load_sentence_vectors(128, 25, -0.5))
    X = np.stack(it.chain(pos_docs, neg_docs))



    pos_docs = np.array([[1, 0] for _ in range(n_pos_docs)])
    neg_docs = np.array([[0, 1] for _ in range(n_neg_docs)])
    y = np.concatenate((pos_docs, neg_docs))

    return X, y



def upload_data(X, y):
    shared_x = theano.shared(X.astype(theano.config.floatX), borrow=False)
    shared_y = theano.shared(y.astype(theano.config.floatX), borrow=False)
    return shared_x, T.cast(shared_y, 'int32')
    



if __name__ == "__main__":
    X_train, y_train = load_random_data(100, 100)

    model = TheanoWordCNNModel(2)
    model.build((128,))
    model.run(X_train, y_train)

