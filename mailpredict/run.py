from .word_cnn import TheanoWordCNNModel
from .preprocessing import load_enron_data, GloveEmbeddings
import sys

import numpy as np

from argparse import ArgumentParser

import theano
import theano.tensor as T
import theano.tensor.nnet


def parse_arguments():
    parser = ArgumentParser()

    parser.add_argument("dataset_path")
    #parser.add_argument("--joint-model", "-j", help = "Run joint prediction", action = "store_true", default = False)
    parser.add_argument("--results-file", help = "Filename for latex results table", default = "results.txt")
    parser.add_argument("--embeddings", help = "Path to the embeddings file. Must be a space seperated plain text file in glove format", default = "glove.6B.50d.txt", type= GloveEmbeddings.fromfile)
    parser.add_argument("--parameter-file", help = "Path to the parameter file. If used together with --tune new hyperparameters are determined and written to the file. If --tune is not specified, the file must exist or an error is raised.", default = None)
    parser.add_argument("--tune", help = "Tune the hyperparameters before running. If neither a parameter file nor this option is specified, default values are used.", default = False, action = "store_true")

    return parser.parse_args()

def run():
    args = parse_arguments()

    run_individual_model(args.dataset_path, args.results_file, args.embeddings, args.parameter_file, args.tune)

import itertools as it

def create_arg_dicts_from_configs(configs):
    sorted_keys = sorted(configs.keys())
    config_option_sizes = list(map(lambda k: len(configs[k]), sorted_keys))

    for indices in it.product(*map(range, config_option_sizes)):
        curr_dict = {}
        for key, curr_idx in zip(sorted_keys, indices):
            curr_dict[key] = configs[key][curr_idx]
        print(curr_dict)

        yield curr_dict

def find_best_hyperparameters(tuning_mailbox, embeddings_dim, epochs = 20):
    configs = {
            "layer_config": [
                (
                    (
                        (1, 1),
                    ),
                ),
                (
                    (
                        (32, 1),
                        (16, 3),
                        (8, 5),
                    ),
                ),
                (
                    (
                        (32, 1),
                        (16, 3),
                        (8, 5),
                    ),
                    (
                        (64, 2),
                        (32, 4),
                    ),
                )

            ],
            "hidden_layer_size": [
                64,
                128,
                256
            ],
            "eta": [0.01, 0.001],
            "beta1": [0.2, 0.5, 0.8],
            "beta2": [0.2, 0.5, 0.8]
        }

    config_scores = {}
    for argdict in create_arg_dicts_from_configs(configs):
        real_argdict = dict(argdict.items())
        real_argdict["max_epochs"] = epochs
        map_sum = 0.0
        for user, data in tuning_mailbox:
            map_sum += run_and_evaluate_model_on_data(data, real_argdict, embeddings_dim)
        print("Avg MAP on Dev", map_sum / len(tuning_mailbox))
        config_scores[tuple(argdict.items())] = map_sum

    best_config = dict(max(config_scores.items(), key = lambda x: x[1])[0])

    print("Best config:", best_config)

    return best_config

import pickle

def run_individual_model(dataset_path, results_file, embeddings, parameter_file = None, should_tune = False):
    all_mailboxes = sorted(load_enron_data(dataset_path, embeddings).items(), key = lambda i: i[1][0].shape[0], reverse = True)

    embeddings_dim = embeddings.dim

    eval_mailboxes = all_mailboxes[0:10:2]

    for user, data in eval_mailboxes:
        print(user, (data[0].shape[0]))
    

    if should_tune:
        tuning_mailbox = all_mailboxes[1:10:2]
        best_config = find_best_hyperparameters(tuning_mailbox, embeddings_dim)
        with open(parameter_file, "wb") as f_out:
            pickle.dump(best_config, f_out)
    elif parameter_file is not None:
        with open(parameter_file, "rb") as f_in:
            best_config = pickle.load(f_in)
    else:
        best_config = {}

    map_scores = []
        
    for user, data in eval_mailboxes:
        map_scores.append((user, run_and_evaluate_model_on_data(data, best_config, embeddings_dim)))

    with open(results_file, "w") as f_results:
        f_results.write("\\begin{tabular}{|l|c|}\n\\hline User & MAP \\\\\\hline\n")
        for user, score in map_scores:
            f_results.write("{}&{}\\\\\n".format(user, score))

        f_results.write("\\hline\\end{tabular}")

def run_and_evaluate_model_on_data(data, model_args, embeddings_dim):
    X_train, y_train, X_test, y_test = data
    model = TheanoWordCNNModel(y_train.shape[1], **model_args)

    model.build((embeddings_dim,))
    model.run(X_train, y_train)
    predictions = model.predict(X_test, y_test)


    return calculate_map(y_test, predictions)


def calculate_map(y_gold, scores, n = None):
    sorted_indices = np.fliplr(np.argsort(scores, axis = 1))

    if n is not None:
        sorted_indices = sorted_indices[:,:n]
    else:
        n = sorted_indices.shape[1]

    top_n_gold_labels = []

    for idx, sorted_row_indes in enumerate(sorted_indices):
        top_n_gold_labels.append(y_gold[idx, sorted_row_indes])

    top_n_gold_labels = np.stack(top_n_gold_labels)

    map_scores = np.zeros(scores.shape[0])
    num_correct_predictions = np.zeros(scores.shape[0])

    total_correct_values = np.sum(top_n_gold_labels, axis = 1)

    for idx in range(n):
        num_correct_predictions += (top_n_gold_labels[:,idx] == 1)
        map_scores += num_correct_predictions / (idx + 1) * (top_n_gold_labels[:,idx] == 1)

    map_scores /= total_correct_values
    map_scores[total_correct_values == 0] = 0.0

    return np.sum(map_scores) / y_gold.shape[0]

