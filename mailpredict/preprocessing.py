import os
import os.path

import email.utils
import time

import math
import numpy as np

from collections import defaultdict

class EnronCorpus:
    def __init__(self, basedir):
        self.basedir = basedir

    def iter_user_mail_collections(self):
        for foldername in os.listdir(self.basedir):
            full_path = os.path.join(self.basedir, foldername)
            if os.path.isdir(full_path):
                yield UserMailbox(foldername, full_path)

class UserMailbox:
    def __init__(self, user_name, path):
        self.user_name = user_name
        self.path = path

    def iter_outbox(self):
        sent_items = os.path.join(self.path, "sent_items")
        sent = os.path.join(self.path, "sent")
        _sent_mail = os.path.join(self.path, "_sent_mail")

        sent_paths = []
        
        if os.path.isdir(sent_items):
            sent_paths.append(sent_items)
          #  sent_path = sent_items
        if os.path.isdir(sent):
            sent_paths.append(sent)
            
#            sent_path = sent
        if os.path.isdir(_sent_mail):
        #    sent_path = _sent_mail
            sent_paths.append(_sent_mail)
            
        if len(sent_paths) == 0:
            return []

        for sent_path in sent_paths:
            for mailfilename in os.listdir(sent_path):
                full_path = os.path.join(sent_path, mailfilename)
                if not os.path.isfile(full_path):
                    continue
                yield Mail.parse(full_path)

class Mail:
    @classmethod
    def parse(self, fname):
        with open(fname, errors = "replace") as f:
            header_items = {}
            prev_header = None
            for line in f:
                line = line.strip()
                if len(line) == 0:
                    break

                items = line.split(":", 1)
                if len(items) == 1:
                    header_items[prev_header] += " " + line
                else:
                    header, value = items
                    header_items[header] = value.strip()
                    prev_header = header

            text_lines = []
            for line in f:
                text_lines.append(line)

        if "To" in header_items.keys():
            recipients = header_items["To"].split()
        else:
            recipients = []

        if "Cc" in header_items.keys():
            recipients.extend(header_items["Cc"].split())

        if "Bcc" in header_items.keys():
            recipients.extend(header_items["Bcc"].split())
            
        sender = header_items["From"]
        date = time.mktime(email.utils.parsedate(header_items["Date"]))

        return Mail(sender, recipients, date, " ".join(text_lines))

    def __init__(self, sender, recipients, date, textual_content):
        self.sender = sender
        self.recipients = recipients
        self.date = date
        self.textual_content = textual_content

def read_enron_train_test(basedir, testsplit = 0.2):
    corpus = EnronCorpus(basedir)

    user_collections = {}

    for user_mailbox in corpus.iter_user_mail_collections():
        sent_messages = list(user_mailbox.iter_outbox())
        ordered_messages = sorted(sent_messages, key = lambda m: m.date)

        splitpoint = int(math.floor(len(ordered_messages) * (1.0 - testsplit)))

        #sent_messages = list(map(lambda m: m.textual_content, sent_messages))
        train_msgs = sent_messages[:splitpoint]
        test_msgs = sent_messages[splitpoint:]

        user_collections[user_mailbox.user_name] = (train_msgs, test_msgs)

    return user_collections

def text_to_embeddings(text, embedding_provider, max_tok_cnt):
    shape = (max_tok_cnt, embedding_provider.dim)
    word_embeddings = np.zeros(shape = shape)
    for idx, token in enumerate(text.split()):
        if idx >= max_tok_cnt:
            break

        embedding = embedding_provider(token)
        word_embeddings[idx,:] = embedding

    return word_embeddings


def class_lists_to_one_hot_matrix(class_lists, num_cls):
#    max_val = max(map(max, filter(lambda l: len(l) > 0, class_lists)))

    res_matrix = np.zeros(shape = (len(class_lists), num_cls + 1))
    for idx, class_list in enumerate(class_lists):
        res_matrix[idx, class_list] = 1
    
    return res_matrix

def prepare_enron_data(user_collections, embedding_provider):
    all_user_data = {}

    for user_name, dataset in user_collections.items():
        user_recipient_id_map = {}
        train_set, test_set = dataset
        
        curr_max_id = 1
        curr_max_tok_cnt = 0

        train_mail_recipient_lists = []
        train_mail_embedded_sentences = []
        for mail in train_set:
            mail_recipient_list = []
            for recipient in mail.recipients:
                recipient = recipient.lower()
                user_id = user_recipient_id_map.get(recipient)
                if not user_id:
                    user_id = curr_max_id
                    curr_max_id += 1
                    user_recipient_id_map[recipient] = user_id

                mail_recipient_list.append(user_id)

            train_mail_recipient_lists.append(mail_recipient_list)
            train_mail_embedded_sentences.append(text_to_embeddings(mail.textual_content, embedding_provider, 500))

        test_mail_recipient_lists = []
        test_mail_embedded_sentences = []
        for mail in test_set:
            mail_recipient_list = []
            for recipient in mail.recipients:
                recipient = recipient.lower()
                user_id = user_recipient_id_map.get(recipient, 0)
                mail_recipient_list.append(user_id)
            
            test_mail_recipient_lists.append(mail_recipient_list)
            test_mail_embedded_sentences.append(text_to_embeddings(mail.textual_content, embedding_provider, 500))

        if len(train_mail_embedded_sentences) > 0 and len(test_mail_embedded_sentences) > 0:
            all_user_data[user_name] = np.stack(train_mail_embedded_sentences), class_lists_to_one_hot_matrix(train_mail_recipient_lists, curr_max_id - 1), np.stack(test_mail_embedded_sentences), class_lists_to_one_hot_matrix(test_mail_recipient_lists, curr_max_id - 1)

    return all_user_data

class RandomEmbeddings:
    def __init__(self, dim):
        self.dim = dim
        self.items = defaultdict(lambda: np.random.uniform(size = (dim,), low = -1.0, high = 1.0))

    def __call__(self, tok):
        return self.items[tok.lower()]

class GloveEmbeddings:
    @staticmethod
    def fromfile(filename):
        embedding_dict = {}
        with open(filename) as f:
            for line in f:
                components = line.strip().split()
                embedding_dict[components[0]] = np.array(list(map(float, components[1:])))
        return GloveEmbeddings(embedding_dict)

    def __init__(self, embeddings):
        self.embeddings = embeddings
        self.dim = next(iter(embeddings.values())).shape[0]

    def __call__(self, tok):
        return self.embeddings.get(tok.lower(), np.zeros(self.dim))


def load_enron_data(basedir, embeddings):
    corpus = read_enron_train_test(basedir)
    return prepare_enron_data(corpus, embeddings)
