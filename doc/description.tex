\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[german]{babel}
\usepackage[T1]{fontenc}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\usepackage{caption}

\usepackage{natbib}
\usepackage{algorithm,algorithmic}

\usepackage{multirow}

\usepackage{graphicx}
\usepackage{csquotes}
\usepackage{graphicx}
\allowdisplaybreaks

\hyphenation{Misch-sys-teme}

\begin{document}

\title{Neural Networks - Projectbericht}
\author{
Julius Steen - Matr. - Nr.: 3219620
}
\date{\today}

\maketitle

\section{Einleitung}

E-Mail Empfänger Vorhersage beschreibt die Vorhersage von möglichen Empfängern einer Mail aus dem vorangegangenen Kommunikationsverhalten eines Nutzers. Sie dient dazu, Benutzer bei der Auswahl von Empfänger zu unterstützen und zu verhindern, dass versehentlich eigentlich intendierte Empfänger vergessen werden (s. z.B. \citet{emailprediction}).

In diesem Projekt wird ein auf Convolutional Neural Networks basierendes System implementiert, das diese Aufgabe auf Grundlage der Inhalte und Empfänger zuvor versendeter Mails erlernen soll.

\section{Frühere Arbeiten}

E-Mail-Empfänger-Vorhersage wird für gewöhnlich als Rankingproblem über die dem Versender zum Zeitpunkt des Versands bekannten E-Mail-Adressen definiert. \citet{desai-dash} teilen bisherige Systeme in drei grobe Kategorien ein: auf Metadaten basierende Systeme, die Kommunikationsmuster verwenden, wie beispielsweise häufige Empfänger von Nachrichten, textbasierte Systeme, die versuchen, Empfänger aus dem Inhalt der Mail bestimmen, und Mischsysteme. Ein Beispiel für letzteres ist \citet{emailprediction}. Ihr System verwendet inhaltliche Ähnlichkeit zu erhaltenen und gesendeten Mails, Statistiken über frühere Kommunikation mit anderen Nutzern und Heuristiken für Begrüßungsformeln, um Features für eine Ranking SVM zu erzeugen.

Die für dieses Projekt verwendeten Convolutional Neural Networks haben bei früheren Experimenten starke Performanz bei Textklassifikationsaufgaben gezeigt (s. z.B. \citet{zhang} für einen Überblick und Analyse). \citet{zhang} gehen soweit, sie als neue Standard-Baseline, ähnlich der Support Vector Machine, zu bezeichnen. Ihre Verwendung in dieser Arbeit ist von daher eine naheliegende Wahl.

\section{Architektur}

Das hier vorgestellte System arbeitet ausschließlich mit den gesendeten E-Mails der Benutzer, sowie mit ihren Adressbücher, sprich der Liste der Adressen, an die sie in der Vergangenheit E-Mails geschrieben haben. Der Inhalt jeder gesendeten E-Mail bildet hierbei je eine Instanz. Die Empfänger der E-Mail definieren die positiven, die restlichen im Addressbuch des Benutzers enthaltenen Adressen die negativen Labels. Da E-Mails mehrere Empfänger haben können, kann eine einzelne Instanz in diesem Prozess mehrere positive Labels erhalten. Für dieses Projekt zählen Empfänger in den \textit{To}-, \textit{Cc}- und \textit{Bcc}-Felder der Mail in die Empfängerliste. Dies entspricht dem \textit{TO+CC+BCC}-Task von \citet{emailrecommend}.
Die so erhaltenen Trainingsbeispiele werden verwendet, um eine Reihe binärer Klassifizierer zu trainieren, deren Konfidenzwerte ein Ranking der potenziellen Empfänger definieren.

Das Vorhersagesystem besteht aus zwei Stufen. Die erste Stufe, das Convolutional Network, folgt einer Standard Convolutional Network Architektur für Textklassifikation, wie beispielsweise bei \citet{zhang} beschrieben. Das Netzwerk erhält Glove Embeddings \citep{glove} mit Dimensionalität 50 als Eingabe. Pro E-Mail werden maximal die ersten 500 Worte in Betracht gezogen. Diese Eingabe wird von darauf folgenden Convolutional Layers transformiert. Layers können dabei Filter unterschiedlicher Größen enthalten. Nach der letzten Konvolution wird eine Max-Pooling-Ebene eingefügt, die den Ausgabevektor jedes Filters auf seine größte Aktivierung reduziert und die bisher variabel lange Ausgabe so auf eine fixe Größe zurückführt. Die Ausgabe des Max-Poolings-Layers wird von einem vollverbundenen Layer weiter transformiert.
Allen Layers ist eine Rectified Linear Unit (ReLU) als nichtlineare Transformation nachgeschaltet.

Die Ausgabe des vollverbundenen Layers dient in der zweiten Stufe als Eingabe für eine Reihe von Sigmoid Neuronen, die als binäre Klassifizierer jeweils die Präsenz oder Abwesenheit eines Empfängers in der Empfängerliste vorhersagen. Als Loss-Funktion kommt für jeden Klassifizierer die binäre Kreuzentropie zum Einsatz. Dieser Loss wird für jeden Klassifizierer aufsummiert, um den Gesamtloss $L$ für ein Trainingsbeispiel zu erhalten.
Diese Architektur erlaubt es den einzelnen Empfängerklassifizierern sich die Informationen aus der CNN-Ebene zu teilen und sorgt gleichzeitig dafür, dass den tieferen Ebenen eine größere Menge Trainingsdaten zur Verfügung steht.
Vorläufige Experimente haben ergeben, dass das System dazu neigt, viele, wenn nicht sogar alle Empfänger mit dem Konfidenzwert $1.0$ vorherzusagen. Dies ist für ein System, welches zielgenau Empfänger vorhersagen soll, nicht wünschenswert. Aus diesem Grund wird für den Vorhersagevektor eine L2-Regularisierung eingeführt. Diese kodiert die Intuition, dass nur eine kleine Menge von Empfängern für eine Nachricht relevant sein sollte.

Die Formel für den Loss ergibt sich demnach wie folgt:
Sei $p_i$ die Vorhersage des Klassifizierers für Empfänger $i$ und $t_i$ eine binäre Variable, die $1$ ist, wenn der Benutzer $i$ Empfänger ist und $0$ sonst. Sei außerdem $M$ die Anzahl der möglichen Empfänger, dann ist $L$ wie folgt definiert:

\begin{displaymath}
L_(p, t) = \frac{1}{M} \sum_i^{M} -(\log(p_i) * t_i + \log(1 - p_i) * (1 - t_i)) + \|p\|^2
\end{displaymath}

Das gesamte System wird mittels Mini-Batch Gradientenabstieg mit ADAM Optimierer \citep{adam} trainiert.

Um das abschließende Ranking zu erhalten, folgt das System \citet{emailrecommend} und verwendet ein Direct Reranking by Classification Schema nach \citet{reranking-algorithms}. Bei diesem Schema definieren die Konfidenzwerte eines Klassifizierers, hier die Aktivierungen der Sigmoid Neuronen, das Zielranking.

\section{Evaluation}

Das Projekt verwendet den Enron-Corpus \citep{enron}, eine Sammlung von Geschäftsemails, zu Trainings- und Evaluationszwecken. 

Zur Erstellung eines Trainings- und Testsets wird für den gesamten Datensatz ein Zeitpunkt gewählt, sodass $20 \%$ aller gesendeten Mails nach diesem Zeitpunkt entstanden sind. Die später entstandenen Mails dienen als Test-Set, während der Rest als Trainingsbeispiel eingesetzt wird. Nutzer, für die keine Trainingsdaten existieren, werden ausgefiltert.
In diesem Projekt werden nur die 10 Benutzer mit den meisten gesendeten Mails im Trainingsset berücksichtigt. Unter den Benutzern wird sowohl ein Evaluationsset, als auch ein Developmentset definiert. Die Benutzer mit grader Position im Ranking nach der Anzahl der versendeten Mails, angefangen von und inklusive 0, sind Teil des Evaluationssets, diejenigen mit ungrader Position Teil des Developmentsets. Diese Auswahl dient dazu, ein Developmentset zu erhalten, das sowohl dieselbe Trennung zwischen vergangenen und zukünftigen E-Mails aufweist, wie das Evaluationsset, als auch möglichst repräsentativ für den gesamten Datensatz ist.

\subsection{Bestimmung der Hyperparameter}

Sowohl die Größe des Hidden Layer, als auch die Struktur der Convolutional Layer werden mittels Grid Search auf dem Developmentset bestimmt. Auf dieselbe Weise werden die Parameter für ADAM festgelegt. Die zu optimierende Metrik ist die Mean Average Precision (MAP) auf dem Developmentset. Auf Grund des großen Suchraums bei dieser Aufgabe wird bei der Parameterbestimmung nur für 20 Epochen trainiert.

Das performanteste System hat einen Convolutional Layer mit nur einem Filter mit Größe 1 und 128 Hidden Units. Dies ist überraschend, da diese Architektur einem einfachen Max-Pooling über die Eingabesequenz mit vorheriger Transformation entspricht. Wir spekulieren, dass dies eine Konsequenz zu geringer Trainingsdatenmengen oder zu kurzer Trainingszeiten sein kann, die verhinderen, dass komplexere Modelle ausreichend optimiert werden können.

\subsection{Ergebnisse}

Die Ergebnisse des getunten Systems auf dem Evaluationsset finden sich in Tabelle \ref{tab:results}. Wir vergleichen die Ergebnisse mit \citet{emailrecommend}, da die dort präsentierte Evaluation Ergebnisse pro Benutzer berichtet, die eine Teilevaluation des Korpus ermöglichen. \citet{emailrecommend} evaluieren sowohl rein textuelle Features, als auch zusätzliche Metadaten-Features. Als Vergleichsgrundlage dienen hier pro Benutzer das beste Ergebnis auf Textfeatures, sowie das insgesamt beste Ergebnis. Zusätzlich wird eine ebenfalls von \citeauthor{emailrecommend} erstellten Zufallsbaseline in den Vergleich einbezogen.
Da das ursprünglich von \citeauthor{emailrecommend} verwendete, gefilterte Datenset nicht mehr zur Verfügung steht, sind die Ergebnisse leider nur bedingt und unvollständig vergleichbar.

\begin{table}
\begin{center}
\begin{tabular}{|l|c||c|c|c|c|}
\hline User & System & Zufall & Text & Gesamt & $|\text{Trainingsdaten}|$ \\\hline
mann-k&0.169 & - & - & - & 7140 \\
dasovich-j&0.094 & - & - & - & 4292 \\
shackleton-s &0.036 & 0.072 & 0.373 & 0.438 & 3525\\
bass-e&0.207 &0.016 & 0.407 & 0.478 & 2424 \\
beck-s&0.171 & 0.120 & 0.237 & 0.329 & 2139 \\
%blair-l&0.0835 & 0.037 & 0.485 & 0.568 & 743 \\
%heard-m&0.0261 & - & - & - & 627\\
%lenhart-m&0.162 & - & - & - & 613\\
%arnold-j&0.0520 & - & - & - & 578\\
%rodrique-r&0.0946 & - & - & - & 576 \\
\hline\end{tabular}
\caption{MAP für die beste Architektur auf dem Developmentset im Vergleich \label{tab:results}}
\end{center}
\end{table}

Insgesamt zeigt sich, dass die Ergebnisse mit nur einer Ausnahme besser sind, als die zufällige Baseline. Allerdings bleiben die Resultate in allen Fällen sowohl hinter den textbasierten Ergebnissen, als auch denen inklusive Metadaten von \citeauthor{emailrecommend} zurück. Wir spekulieren, dass der Umfang der Korpora zu gering ist und damit die zur Verfügung stehenden Trainingsdaten nicht ausreichen, um gute Ergebnisse zu erzielen. Genauere Aussagen bedürfen allerdings einer deutlich detaillierteren Untersuchung, die außerhalb des Rahmens dieses Projekts liegt.

\section{Zusammenfassung und zukünftige Arbeiten}

Das System zeigt in den Experimenten keine befriedigende Performanz und kann die Ergebnisse des als Vergleich herangezogenen Systems nicht erreichen. Trotzdem zeigt sich, dass das System prinzipiell auf die Daten angepasste Funktionen erlernt und bessere Ergebnisse erzielt, als sich allein durch Zufall erklären ließe.

Zukünftige Wege zur Weiterentwicklung können in zwei Richtungen gehen. Zum einen kann das Klassifizierungssystem selbst verbessert werden. Beispielsweise könnte der CNN-Teil des Setups nicht nur zwischen Empfängern geteilt werden, sondern in einem Multi-Task-Setting auch zwischen den einzelnen Benutzern. Dies könnte dabei helfen, stärkere CNNs zu trainieren.
Die zweite Möglichkeit der Verbesserung ist die Integration von zusätzlichen Informationen. Dabei ist es denkbar, beispielsweise inhaltliche Ähnlichkeit mit empfangenen E-Mails zu verwenden, um Vorhersagen zu treffen, ähnlich wie bei \citet{emailprediction}. Weiterhin haben \citet{emailprediction} demonstriert, dass Metainformationen hilfreich beim Empfängerranking sind.

\bibliographystyle{plainnat}
\bibliography{bib}

\end{document}
